'use strict';
angular.module('dashboard', [
        'ionic',
        'ngCordova',
        'ui.router',
        // TODO: load other modules selected during generation
    ])
    .config(function($stateProvider, $ionicConfigProvider) {

        $ionicConfigProvider.backButton.previousTitleText(false).text('');

        // ROUTING with ui.router
        $stateProvider
            .state('dashboard', {
                url: '/dashboard',
                abstract: true,
                templateUrl: 'dashboard/templates/menu.html',
                controller: "MenuCtrl"
            })
            .state('dashboard.perfil', {
                url: '/perfil',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/perfil.html',
                        controller: 'PerfilCtrl'
                    }
                }
            })
            .state('dashboard.ubicacion', {
                url: '/ubicacion',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/ubicacion.html',
                        controller: 'UbicacionCtrl'
                    }
                }
            })
            .state('dashboard.categorias', {
                url: '/categorias',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/categorias.html',
                        controller: 'CategoriasCtrl'
                    }
                }
            })
            .state('dashboard.promociones', {
                url: '/promociones',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/promociones.html',
                        controller: 'PromocionesCtrl'
                    }
                }
            })
            .state('dashboard.puntos', {
                url: '/puntos',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/puntos.html',
                        controller: 'PuntosCtrl'
                    }
                },
                params: {
                    id_categoria: null,
                    busqueda: null,
                }
            })
            .state('dashboard.punto', {
                url: '/punto/:id_punto',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/punto.html',
                        controller: 'PuntoCtrl'
                    }
                }
            })
            .state('dashboard.servicio', {
                url: '/servicio',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/servicio.html',
                        controller: 'ServicioCtrl'
                    }
                }
            })
            .state('dashboard.historico', {
                url: '/historico',
                views: {
                    'services': {
                        templateUrl: 'dashboard/templates/historico.html',
                        controller: 'HistoricoCtrl'
                    }
                }
            })

    });