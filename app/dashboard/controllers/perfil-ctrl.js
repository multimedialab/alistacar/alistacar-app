'use strict';
angular.module('dashboard').controller('PerfilCtrl', function($scope, $server, $usuario, $message) {

  $scope.usuario = $usuario;

  function validarTelefono(telefono) {
    return /^\d{10}$/.test(telefono);
  }

  $scope.actualizarusuario = function() {
    console.log($scope.usuario.nombre, $scope.usuario.email);
    console.log($scope.usuario.telefono, validarTelefono($scope.usuario.telefono));
    if ($scope.usuario.nombre == "" || $scope.usuario.email == "" || !validarTelefono($scope.usuario.telefono)) {
      return $message.alert("Datos incorrectos, verifique los datos y el número de teléfono celular.");
    }

    $server.actualizarDatos($scope.usuario, function() {
      $message.alert("Datos guardados correctamente");
    });
  }

  $scope.cambiarContrasena = function(nueva, confirmacion) {
    if (nueva && confirmacion && nueva === confirmacion) {
      $server.actualizarContrasena({
        id: $usuario.id,
        contrasena: confirmacion
      }, function() {
        $message.alert("La contraseña ha sido modificada exitosamente.");
      });
    } else {
      $message.alert("Las contraseñas no coinciden, por favor escribalas nuevamente.");
    }
  }

});
