'use strict';
angular.module('dashboard').controller('PuntosCtrl', function ($scope, $state,$ionicLoading, $stateParams, $server, $message, $commons, $cordovaActionSheet, $cordovaGeolocation, _config, $ionicHistory, $status) {

    $scope.params = $stateParams;
    $scope.subcategoria = "Todas las categorías";

    $scope.cms = _config.cms;

    $server.promociones(function (promociones) {
        $scope.promociones = promociones;
    });

    $server.banners(function (banners) {
        $scope.banners = banners;
    });

    $scope.$on("$ionicSlides.sliderInitialized", function (event, data) {
        $scope.promos = data.promos;
    });
    $scope.slideOptions = {
        autoplay: 3000
    }

    if ($scope.params.id_categoria) {

    }

    $scope.swipeLeft = function () {
        $ionicHistory.goBack();
    }

    $server.categorias($scope.params.id_categoria, function (subcategorias) {

        console.log('iiiiiiiiiiiii->>',$scope.params.id_categoria)
        if($scope.params.id_categoria==98){    
            console.log('entro al 98')        
            $state.go('dashboard.promociones');
        }else{
        $scope.subcategorias = subcategorias;
        }
        $scope.actions = ["Todas las categorías"];
        for (var i in subcategorias) {
            $scope.actions.push(subcategorias[i].nombre);
        }
        $commons.apply($scope);
    });

    $scope.getPuntos = function (highAcurracy, timeout, nocargarenerror) {

        highAcurracy = highAcurracy || false;
        timeout = timeout || 5000;
        nocargarenerror = nocargarenerror || false;

        if (!nocargarenerror) {
        cordova.plugins.diagnostic.isLocationAvailable(function(available){
                console.log('que paso');
                if (!available){
                    var title  = "Activar GPS";
                    var message= "Para encontrar su posición se necesita activar el GPS ¿Desea ir a activarlo?";

                    $message.confirm(title, message).then(function(){
                        cordova.plugins.diagnostic.switchToLocationSettings();
                    })
                }
                
            }, function(error){
                //console.error("The following error occurred: "+error);
            });
        }
        
        $scope.buscando_posicion = true;
        navigator.geolocation.getCurrentPosition(function (position) {
            console.log("---->>$$$encontramos tu posicion", position.coords);
            $scope.buscando_posicion = false;
            $scope.params.latitud  = position.coords.latitude;
            $scope.params.longitud = position.coords.longitude;

            if ($scope.params.id_categoria) {
                console.log("---->>$$$encontramos categorias", position.coords);
                $server.puntos({
                    categoria: $scope.params.id_categoria,
                    latitud: $scope.params.latitud,
                    longitud: $scope.params.longitud
                }, function (puntos) {
                    console.log("nuevos puntos",puntos);
                    $scope.puntos = {};
                    $scope.puntos = puntos;
                    $commons.apply($scope);
                });
            } else {
                $server.buscardor({
                    busqueda: $scope.params.busqueda,
                    latitud: $scope.params.latitud,
                    longitud: $scope.params.longitud
                }, function (puntos) {
                    console.dir(puntos);
                    $scope.puntos = puntos;
                    $commons.apply($scope);
                });
            }
        }, function (error) {
            
            $message.alert('No podemos determinar la ubicación, activa el GPS o intenta ubicarte en un lugar con vista al cielo abierto',"long");
            if (!nocargarenerror) {
                console.log("RECARGANDO--------------<<((((");
                $ionicLoading.hide();
                if ($scope.params.id_categoria) {
                    $server.puntos({
                        categoria: $scope.params.id_categoria,
                        latitud: "6.2520414",
                        longitud: "-75.5706361,17"
                    }, function (puntos) {
                        console.log(puntos);
                        $scope.puntos = puntos;
                        $commons.apply($scope);
                    });
                } else {
                    $server.buscardor({
                        busqueda: $scope.params.busqueda,
                        latitud: "6.2520414",
                        longitud: "75.5706361,17"
                    }, function (puntos) {
                        console.dir(puntos);
                        $scope.puntos = puntos;
                        $commons.apply($scope);
                    });
                }
            }
            nocargarenerror = true;
            highAcurracy = !highAcurracy;
            timeout += 6000;
            $scope.getPuntos(highAcurracy, timeout,nocargarenerror);
        }, {
                enableHighAccuracy: highAcurracy,
                timeout: timeout,
                maximumAge: 60000
            });
    }


    $scope.cambiarCategoria = function () {
        if ($scope.actions.length === 0) {
            $message.alert("No existen categorias para este servicio");
            return;
        }

        $cordovaActionSheet.show({
            title: 'Seleccione una categoría',
            buttonLabels: $scope.actions,
            androidTheme: 5,
            androidEnableCancelButton: true,
            addCancelButtonWithLabel: "Cancelar"
        }).then(function (index) {
            if ($scope.actions.length < index) {
                return;
            } else if (index == 1) {
                $scope.subcategoria = "Todas las categorías";
                $scope.getPuntos($scope.params.id_categoria);
            } else {
                $scope.subcategoria = $scope.subcategorias[index - 2].nombre;
                $server.puntosSubcategoria({
                    categoria: $scope.subcategorias[index - 2].id_categoria,
                    latitud: $scope.params.latitud,
                    longitud: $scope.params.longitud
                }, function (puntos) {
                    $scope.puntos = puntos;
                    $commons.apply($scope);
                });
            }
        });
    }

    $scope.getPuntos();

});