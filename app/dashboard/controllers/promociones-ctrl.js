'use strict';
angular.module('dashboard').controller('PromocionesCtrl', function($scope, $server, $ionicLoading, $status) {
    //$ionicLoading.show();

    $server.promociones(function(promociones) {
        $scope.promociones = promociones;
        //$ionicLoading.hide();
    });

});