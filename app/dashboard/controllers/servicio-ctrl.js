"use strict";
angular
  .module("dashboard")
  .controller("ServicioCtrl", function (
    $scope,
    $server,
    $usuario,
    $commons,
    $status,
    $rootScope
  ) {
    $scope.codigo = $usuario.telefono.substr($usuario.telefono.length - 4);

    getServiciosUsuario();

    if (!$rootScope.activeServicesListener) {
      io.socket.on("connect", getServiciosUsuario);
      $rootScope.activeServicesListener = true;
    }

    function getServiciosUsuario() {
      console.log("onConnect ServicioCtrl");
      $server.serviciosUsuario(
        $usuario.id,
        handleGetServices,
        handleOnServiceUpdate
      );
    }

    function handleGetServices(servicios) {
      $scope.servicios = servicios;
      $commons.apply($scope);
      console.log("estos son", servicios);
    }

    function handleOnServiceUpdate(servicio) {
      for (var i in $scope.servicios) {
        if ($scope.servicios[i].id_servicio == servicio.id_servicio) {
          if (servicio.quien) {
            $scope.servicios.splice(i, 1);
          } else {
            $scope.servicios[i] = servicio;
          }
          $commons.apply($scope);
        }
      }
    }

    $scope.cancelarServicio = function (id_servicio, index) {
      $server.cancelar(id_servicio, function () {
        console.log("valor index", index);
        $scope.servicios.splice(index, 1);
      });
    };
  });
