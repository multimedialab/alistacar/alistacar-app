"use strict";
angular
  .module("dashboard")
  .controller("CategoriasCtrl", function (
    $scope,
    $stateParams,
    $server,
    $commons,
    $state,
    $ionicLoading,
    $ionicPopup,
    $status,
    $rootScope
  ) {
    var ctrl = this;
    $scope.params = $stateParams;

    getServices();

    if (!$rootScope.activeServiceTypes) {
      io.socket.on("connect", getServices);
      $rootScope.activeServiceTypes = true;
    }

    function getServices() {
      console.log("on connect interno");
      $server.servicios(function (categorias) {
        $scope.services = categorias;
        $commons.apply($scope);
        console.log(categorias);
      });
    }
  });
