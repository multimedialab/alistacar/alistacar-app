'use strict';
angular.module('dashboard').controller('PuntoCtrl', function ($scope, $state, $stateParams, $server, $message, $commons, $usuario, $filter, $cordovaDatePicker, $ionicModal, _config, $status) {

    $scope.tab = false;
    $scope.cms = _config.cms;
    $scope.categorias = $stateParams.categorias;
    $scope.servicio = {
        usuario: $usuario.id
    }



    $message.loading();

    $server.punto($stateParams.id_punto, function (driver) {
        $scope.driver = driver;

        $scope.driver.puntaje = 0;
        for (var i in $scope.driver.comentarios) {
            $scope.driver.puntaje += $scope.driver.comentarios[i].calificacion;
        }
        $scope.driver.puntaje = Math.round(($scope.driver.puntaje / $scope.driver.comentarios.length) * 10) / 10;
        $scope.driver.puntaje = (isNaN($scope.driver.puntaje)) ? null : $scope.driver.puntaje;

        $commons.apply($scope);
    });

    $scope.currencyValue = function (n) {
        return n.toFixed(2).replace(/(\d)(?=(\d{3})+\.)/g, '$1,').split('.')[0];
    }

    $scope.horarios = function () {
        var mensaje = "";

        if (!$scope.driver.horario)
            return $message.alert("No hay horarios disponibles");
        if ($scope.driver.horario.lunes)
            mensaje += "Lunes: " + $scope.driver.horario.lunes.hora_inicio + " - " + $scope.driver.horario.lunes.hora_fin + "\n";
        if ($scope.driver.horario.martes)
            mensaje += "Martes: " + $scope.driver.horario.martes.hora_inicio + " - " + $scope.driver.horario.martes.hora_fin + "\n";
        if ($scope.driver.horario.miercoles)
            mensaje += "Miércoles: " + $scope.driver.horario.miercoles.hora_inicio + " - " + $scope.driver.horario.miercoles.hora_fin + "\n";
        if ($scope.driver.horario.jueves)
            mensaje += "Jueves: " + $scope.driver.horario.jueves.hora_inicio + " - " + $scope.driver.horario.jueves.hora_fin + "\n";
        if ($scope.driver.horario.viernes)
            mensaje += "Viernes: " + $scope.driver.horario.viernes.hora_inicio + " - " + $scope.driver.horario.viernes.hora_fin + "\n";
        if ($scope.driver.horario.sabado)
            mensaje += "Sábado: " + $scope.driver.horario.sabado.hora_inicio + " - " + $scope.driver.horario.sabado.hora_fin + "\n";
        if ($scope.driver.horario.domingo)
            mensaje += "Domingo: " + $scope.driver.horario.domingo.hora_inicio + " - " + $scope.driver.horario.domingo.hora_fin + "\n";
        if ($scope.driver.horario.festivo)
            mensaje += "Festivos: " + $scope.driver.horario.festivo.hora_inicio + " - " + $scope.driver.horario.festivo.hora_fin;

        $message.popup('Horarios', mensaje);
    }

    $scope.comoLlegar = function () {

        navigator.geolocation.getCurrentPosition(function (position) {
            launchnavigator.navigate([$scope.driver.latitud, $scope.driver.longitud], {
                start: position.coords.latitude + ", " + position.coords.longitude
            });
        });

    }

    $scope.agendar = function (item) {
        if (ionic.Platform.platform() == 'android')
            agenda_android(item);
        else if (ionic.Platform.platform() == 'ios')
            agenda_ios(item);
    }

    $scope.solicitar = function (item) {

        if (item.precio == null)
            item.descuento = 0;
        console.log("hola por que tan mona")

        var mensajeConfirmacion = "Nombre: " + item.nombre + "\nFecha: " + $filter('date')(new Date(), "yyyy-MM-dd HH:mm") + "\n";
        mensajeConfirmacion += (item.precio) ? "Valor: $" + item.precio : "";
        mensajeConfirmacion += (item.descuento) ? "xDescuento: " + item.descuento + "%" : "";

        $message.confirm("Confirmación de servicio", mensajeConfirmacion, "Solicitar", "Cancelar")
            .then(function (res) {
                $scope.servicio.fecha = new Date().getTime();
                $scope.servicio.servicio = item.id_servicio;
                $scope.servicio.id_usuario = $usuario.id;

                $server.solicitar($scope.servicio);
            });
    }

    $ionicModal.fromTemplateUrl('dashboard/templates/comentarios.html', {
        scope: $scope,
        animation: 'slide-in-up'
    }).then(function (modal) {
        $scope.modal = modal;
    });

    $scope.comentarios = function () {
        $scope.modal.show();
    }

    function agenda_android(item) {
        $cordovaDatePicker.show({
            date: new Date(),
            mode: 'date',
            allowOldDates: false,
            allowFutureDates: true,
            androidTheme: datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
        }).then(function (dia) {
            if (!dia) return;
            dia.setHours(new Date().getHours());
            dia.setMinutes(new Date().getMinutes());
            $cordovaDatePicker.show({
                date: dia,
                mode: 'time',
                allowOldDates: false,
                allowFutureDates: true,
                androidTheme: datePicker.ANDROID_THEMES.THEME_DEVICE_DEFAULT_LIGHT
            }).then(function (hora) {
                dia.setHours(hora.getHours());
                dia.setMinutes(hora.getMinutes());
                var mensajeConfirmacion = "Nombre: " + item.nombre + "\nFecha: " + $filter('date')(dia, "yyyy-MM-dd HH:mm") + "\n";

                mensajeConfirmacion += (item.precio)    ? "Valor: $" + item.precio : "";
                mensajeConfirmacion += (item.descuento) ? "Descuento: " + item.descuento + "%" : "";
                $message.confirm("Confirmación de servicio", mensajeConfirmacion, "Solicitar", "Cancelar").then(function (res) {
                    $scope.servicio.fecha = dia.getTime();
                    $scope.servicio.servicio = item.id_servicio;
                    $scope.servicio.id_usuario = $usuario.id;

                    $server.solicitar($scope.servicio);

                });
            });
        });
    }

    function agenda_ios(item) {
        $cordovaDatePicker.show({
            date: new Date(),
            mode: 'datetime',
            allowOldDates: false,
            allowFutureDates: true
        }).then(function (dia) {
            if (!dia) return;
            var mensajeConfirmacion = "Nombre: " + item.nombre + "\nFecha: " + $filter('date')(dia, "yyyy-MM-dd HH:mm") + "\n";
            mensajeConfirmacion += (item.precio)    ? "Valor: $" + item.precio : "";
            mensajeConfirmacion += (item.descuento) ? "Descuento: " + item.descuento + "%" : "";
            $message.confirm("Confirmación de servicio", mensajeConfirmacion, "Solicitar", "Cancelar").then(function (res) {
                $scope.servicio.fecha = dia.getTime();
                $scope.servicio.servicio = item.id_servicio;
                $scope.servicio.id_usuario = $usuario.id;

                $server.solicitar($scope.servicio);

            });
        });
    }

});