'use strict';
angular.module('dashboard').controller('UbicacionCtrl', function($scope, $state, $commons, $server, $message, $cordovaActionSheet, $cordovaGeolocation) {

    $scope.ciudad = "Selecciona tu ciudad";
    $scope.ubicacion = {};

    $scope.server = $server;

    $server.ciudades(function(ciudades) {
        $scope.ciudades = ciudades;
        $commons.apply($scope);
    });

    function tryConnect() {
        return $message.alert("No podemos conectarnos con el servidor de Alistacar", 'long', 'bottom');
    }

    $scope.ubicacionActual = function() {
        if ($status.checkConnection() != 'NONE') {
            $message.loading("Cargando ubicación");
            navigator.geolocation.getCurrentPosition(function(position) {
                $state.go('dashboard.categorias', {
                    latitude: position.coords.latitude,
                    longitude: position.coords.longitude
                });
            }, function(error) {
                $message.alert("Ha ocurrido un error obteniendo la ubicación, por favor verifique que el sensor de Ubicación está activo e intentelo de nuevo.", 'long');
                $message.hide();
                $state.go('dashboard.categorias', {
                    latitude: 4.641389,
                    longitude: -74.092455
                });
            }, {
                enableHighAccuracy: false,
                timeout: 5000
            });
        } else {
            $status.killApp();
        };
    }

    $scope.seleccionarCiudad = function() {
        if (!$server.estado)
            return tryConnect();

        var ciudades = [];
        for (var i in $scope.ciudades) {
            ciudades.push($scope.ciudades[i].nombre);
        }
        $cordovaActionSheet.show({
            title: '¿En qué ciudad te encuentras?',
            buttonLabels: ciudades,
            androidTheme: 5,
            addCancelButtonWithLabel: "Cancelar"
        }).then(function(index) {
            if (!$scope.ciudades[index - 1]) {
                return
            } else {
                $scope.ciudad = $scope.ciudades[index - 1].nombre;
                $scope.ubicacion.ciudad = $scope.ciudades[index - 1];
            }
        });
    }

    $scope.buscarPorDireccion = function() {
        if (!$server.estado)
            return tryConnect();

        if (!$scope.ubicacion.ciudad)
            return $message.alert("Por favor seleccione una ciudad");
        else if (!$scope.ubicacion.direccion || $scope.ubicacion.direccion === "")
            return $message.alert("Por favor escribe una dirección válida");

        $message.loading("Cargando ubicación");

        var direccion = $scope.ubicacion.direccion.replace("#", "%23") + "," + $scope.ubicacion.ciudad.nombre + " Colombia";

        $server.coordenadas(direccion, function(coords) {
            if (!coords) {
                $message.alert("La dirección ingresada es inválida");
            } else {
                $state.go('dashboard.categorias', {
                    latitude: coords.latitude,
                    longitude: coords.longitude
                });
            }
        });
    }

});