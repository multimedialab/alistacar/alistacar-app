"use strict";
angular
  .module("dashboard")
  .controller("HistoricoCtrl", function (
    $scope,
    $commons,
    $server,
    $usuario,
    $rootScope
  ) {
    $scope.servicios = [];

    getHistorico();

    if (!$rootScope.historicoListener) {
      io.socket.on("connect", getHistorico);
      $rootScope.historicoListener = true;
    }

    function getHistorico() {
      $server.historico($usuario.id, function (servicios) {
        console.log("este es el hist", servicios);
        $scope.servicios = servicios || [];
        $commons.apply($scope);
      });
    }
  });
