'use strict';
angular.module('dashboard').controller('MenuCtrl', function($scope, $state, $usuario, $commons, $ionicHistory) {

    $scope.nombre = $usuario.nombre;
    $commons.apply($scope);

    $scope.canGoback = $ionicHistory.backView();
    console.log('here---',$scope.canGoback);

    $scope.logout = function() {
        $usuario.logout();
    }

    $scope.goBack = function() {
        $state.go('dashboard.categorias', {}, { reload: true });
    }

});