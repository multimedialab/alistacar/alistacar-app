'use strict';
angular.module('Alistacar', [
    'main',
    'login',
    'dashboard',
    'servicios'
]).run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        }
    });
});