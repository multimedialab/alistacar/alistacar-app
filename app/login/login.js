'use strict';
angular.module('login', [
        'ionic',
        'ngCordova',
        'ui.router',
        // TODO: load other modules selected during generation
    ])
    .config(function($stateProvider) {

        $stateProvider.state('login', {
            url: '/login',
            templateUrl: 'login/templates/login.html',
            controller: 'LoginCtrl'
        });

        $stateProvider.state('register', {
            url: '/register',
            templateUrl: 'login/templates/register.html',
            controller: 'RegisterCtrl'
        });

        $stateProvider.state('terms', {
            url: '/terms',
            templateUrl: 'login/templates/terms.html',
            controller: 'TermsCtrl as ctrl'
        });

    });