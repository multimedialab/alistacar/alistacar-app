'use strict';
angular.module('login').controller('TermsCtrl', function($scope, $ionicHistory) {

    var ctrl = this;

    ctrl.goBack = function() {
        $ionicHistory.goBack();
    };

});