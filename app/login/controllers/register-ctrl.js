'use strict';
angular.module('login').controller('RegisterCtrl', function($scope, $state, $usuario, $message, $push) {

    $scope.usuario = {};
    $scope.confirmacion = null;

    function validateEmail(email) {
        var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
        return re.test(email);
    }

    function validarTelefono(telefono) {
        return /^\d{10}$/.test(telefono);
    }

    $scope.registrarse = function() {
        if (!$scope.usuario.nombre || !$scope.usuario.email || !$scope.usuario.telefono || !$scope.usuario.password || !$scope.usuario.confirmacion) {
            return $message.alert("Por favor complete todos los campos");
        } else if (!validarTelefono($scope.usuario.telefono)) {
            return $message.alert('Por favor ingrese un teléfono válido');
        } else if (!validateEmail($scope.usuario.email)) {
            return $message.alert('Por favor ingrese un email válido');
        } else if ($scope.usuario.password != $scope.usuario.confirmacion) {
            $message.alert('Las contraseñas no coinciden, ingreselas nuevamente');
            $scope.usuario.password = "";
            $scope.usuario.confirmacion = "";
            return;
        }

        // delete $scope.usuario.confirmacion;

        $message.loading();

        $usuario.registro($scope.usuario, function(res) {
            $message.hide();
            if (res) {
                $push.registro(function() {
                    $state.go('dashboard.categorias');
                });
            } else {
                $message.popup('Alerta', 'Ya tienes una cuenta registrada con el email proporcionado, puede estar registrada con facebook, de lo contrario solicita tu contraseña.');
            }
        });

    }

});