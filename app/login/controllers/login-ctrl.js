'use strict';
angular.module('login').controller('LoginCtrl', function($scope, $state, $ionicHistory, $ionicLoading, $usuario, $message, $push) {

    $scope.usuario = {};

    function obtenerEmail(callback) {
        $message.hide();
        $message.prompt('Complete sus datos', 'Por favor ingrese correo electrónico', '', 'Aceptar', 'Cancelar').then(function(email) {
            var EMAIL_REGEXP = /[a-z0-9!#$%&'*+=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/g;
            if (EMAIL_REGEXP.test(email)) {
                callback(email);
            } else {
                $message.alert("El coreo electrónico es inválido, por favor ingreselo correctamente", "short", "top");
                obtenerEmail(callback);
            }
        });
    }

    function obtenerTelefono(callback) {
        $message.hide();
        $message.prompt('Complete sus datos', 'Por favor ingrese su número telefónico para completar el registro', '', 'Aceptar', 'Cancelar').then(function(telefono) {
            if ((/^\d{10}$/.test(telefono))) {
                callback(telefono);
            } else {
                $message.alert("El número es inválido, por favor ingrese un numero de celular sin espacios ni caracteres especiales", "short", "top");
                obtenerTelefono(callback);
            }
        });
    }

    $scope.ingresarConFacebook = function() {
        facebookConnectPlugin.login(['public_profile', 'email'], function(data) {
            facebookConnectPlugin.api(data.authResponse.userID + "/?fields=name,email", ["email"], function(response) {
                if (response.email) {
                    obtenerTelefono(function(telefono) {
                        response.telefono = telefono;
                        $usuario.facebook(response, function(res) {
                            if (!res) {
                                $message.hide();
                                console.log(res);
                                $message.alert("No pudimos acceder a tu cuenta, por favor intentalo nuevamente");
                            } else {
                                $message.hide();
                                $push.registro(function() {
                                    $ionicHistory.clearCache().then(function() {
                                        $state.go('dashboard.categorias', {}, { reload: true });
                                    });
                                });
                            }
                        });
                    });
                } else {
                    obtenerEmail(function(email) {
                        response.email = email;
                        obtenerTelefono(function(telefono) {
                            response.telefono = telefono;
                            $usuario.facebook(response, function(res) {
                                if (!res) {
                                    $message.hide();
                                    $message.alert("No pudimos acceder a tu cuenta, por favor intentalo nuevamente");
                                } else {
                                    $message.hide();
                                    $push.registro(function() {
                                        $ionicHistory.clearCache().then(function() {
                                            $state.go('dashboard.categorias', {}, { reload: true });
                                        });
                                    });
                                }
                            });
                        });
                    });
                }
            }, function(error) {
                console.log('2', error);
                $message.popup("Error", "Tuvimos problemas al ingresar a tu cuenta, por favor intentalo nuevamente");
            });
        }, function(error) {
            console.log('1', error);
            $message.popup("Error", "Tuvimos problemas al ingresar a tu cuenta, por favor intentalo nuevamente");
        });
    };

    $scope.ingresarConCorreo = function() {
        if (!$scope.usuario.email || $scope.usuario.email == "" || !$scope.usuario.password || $scope.usuario.password == "") {
            return $message.alert("Por favor complete todos los campos.");
        }

        $usuario.login($scope.usuario, function(res) {
            console.log('resp login ==>', res)
            if (res)
                $push.registro(function() {
                    $ionicHistory.clearCache().then(function() {
                        $ionicHistory.clearHistory();
                        $ionicLoading.hide();
                        $state.go('dashboard.categorias', {}, { reload: true });
                    });
                });
            else {
                $ionicLoading.hide();
                $message.popup('El usuario o la contraseña son incorrectos, intentalo nuevamente.',"El usuario o la contraseña son incorrectos, intentalo nuevamente.");
            }
        });
    }

    $scope.olvidoContrasena = function() {
        $message.prompt("¿Olvidó su contraseña?", "Por favor ingrese su email").then(function(email) {
            $usuario.olvidoContrasena(email, function(res) {
                if (res)
                    $message.alert("Hemos enviado un correo electrónico con la contraseña", "long");
                else
                    $message.alert("El email ingresado no se encuentra registrado");
            });
        });
    }

});