'use strict';
angular.module('servicios', [
  'ionic',
  'ngCordova',
  'ui.router',
])
.config(function ($stateProvider) {
  $stateProvider.state('calificacion', {
    url: '/calificacion',
    templateUrl: 'servicios/templates/calificacion.html',
    controller: 'CalificacionCtrl',
    params: { servicio: null }
  });
});
