'use strict';
angular.module('servicios').controller('CalificacionCtrl', function($scope, $state, $stateParams, $server, $message, $ionicHistory, _config) {

    $scope.servicio = $stateParams.servicio;
    $scope.cms = _config.cms;

    $scope.respuesta = {
        contenido: null,
        calificacion: 0,
        id_punto_de_servicio: $scope.servicio.id_punto_de_servicio,
        id_servicio: $scope.servicio.id_servicio
    };

    $scope.terminar = function() {
        if ($scope.respuesta.puntaje == 0)
            return $message.alert("Para terminar el servicio debe puntuar al proveedor");

        $server.calificacion($scope.respuesta, function() {
            $state.go('dashboard.categorias');
        });
    }

    $ionicHistory.removeBackView();

});