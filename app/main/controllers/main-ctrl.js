'use strict';
var main = angular.module('main');

main.controller('MainCtrl', function($log, $scope, $localStorage, $rootScope, $cordovaNetwork, $ionicPopup, $state) {

    // body...
    $scope.checkConnection = $localStorage;
    $scope.genericText = {};

    document.addEventListener("deviceready", function() {

        var net = {};

        $scope.deviceReady = true;
        $scope.failconnectionDisplayed = false;

        net.type = $cordovaNetwork.getNetwork();
        net.isOnline = $cordovaNetwork.isOnline();
        net.isOffline = $cordovaNetwork.isOffline();

        $localStorage.connection = net.isOnline;

        // listen for Online event
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState) {
            $localStorage.connection = true;
            $scope.failconnectionDisplayed = true;
            $scope.$apply();
        });

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState) {
            $localStorage.connection = false;
            $scope.$apply();
        });
    });

});