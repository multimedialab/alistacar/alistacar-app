"use strict";
angular
  .module("main")
  .service("$server", function (
    _config,
    $usuario,
    $interval,
    $timeout,
    $state,
    $rootScope,
    $message,
    $ionicPopup,
    $status,
    $ionicLoading,
    $PilaMensajes,
    $q
  ) {
    var server = this;
    server.timer;

    $rootScope.$on("appforeground", function (event, args) {
      console.log("appforeground");
      server.forceReconnect();
    });

    $rootScope.$on("networkonline", function (event, args) {
      console.log("onNetworkOnline");
      server.forceReconnect();
    });

    io.socket.on("connect", function () {
      console.log("onConnect");
      $message.alert("Ahora estás conectado", "long", "bottom");
      server.estado = true;
      $rootScope;

      try {
        //if ($state.$current && $state.$current.name) $state.reload();
      } catch (e) {
        console.log("fallo la recarga por conexion a internet");
      }
    });

    io.socket.on("disconnect", function () {
      server.estado = false;
      console.log("nos desconectamod onDisconnect");
      $message.alert("Reconectando", "long", "bottom");

      $ionicLoading.hide();

      angular.forEach($PilaMensajes.items, function (value, key) {
        console.log("Lo siento hubo un error en el servidor");
        $message.alert(
          "Lo siento hubo un error en la solicitud, intente más tarde",
          ""
        );
      });
      $PilaMensajes.eliminarCola();
      $message.hide();
    });

    io.socket.on("message", function (res) {
      $state.go("calificacion", { servicio: res });
    });

    io.socket.on("error", function (res) {
      console.log("error ");
    });

    io.socket.on("connect_error", function (res) {
      console.log("esta es la conexion del error", "connection_error");
      server.forceReconnect();
    });

    io.socket.on("connecting", function (res) {
      console.log("connection_error");
    });

    server.forceReconnect = function () {
      // https://stackoverflow.com/questions/41400510/sails-socket-wont-reconnect
      io.socket._raw.disconnect();
      io.socket._raw.connect();
    };

    /**
     *
     * @param {string} apimethod ex: 'solicitar'
     * @param {*} payload
     * @param {string:post,get,post,put} verb
     */
    server.enviarPeticionAPI = function (apimethod, payload, verb) {
      // perform some asynchronous operation, resolve or reject the promise when appropriate.
      return $q(function (resolve, reject) {
        //validar si tenemos un socket abierto para comunicarnos
        if (!io.socket.isConnected()) {
          $message.alert(
            "No se pudo realizar la solicitud intenta mas tarde",
            "long",
            "bottom",
            function () {
              $state.go("dashboard.servicio");
            }
          );
          reject("No se pudo realizar la solicitud intenta mas tarde");
          return; //internal
        }

        //Mostrando un estado al usuario
        console.log("mostrando el spinner");
        $ionicLoading.show({
          template: "<ion-spinner></ion-spinner>Cargando..",
        });

        //Agregar el mensaje  a la cola
        var id = $PilaMensajes.agregar(payload);

        //miramos si tenemos el verbo correcto
        if (!verb) {
          verb = "get";
        }
        var allowed_verbs = ["get", "post", "put"];
        if (
          allowed_verbs.indexOf(verb) === -1 ||
          allowed_verbs.indexOf(verb) === false
        ) {
          reject("Not allowed verb");
        }

        //Enviar el mensaje
        server.messageid = id;
        //  Ejemplo io.socket.post('/cancelar', { id: id_servicio }, function(response) {});
        io.socket[verb](apimethod, payload, function (response, jwt) {
          /*  console.log('me respondieron',response.messageid); */

          //Respuesta recibida ya sea mala o buena la podemos quitar de la pila
          if (response.messageid) $PilaMensajes.eliminar(response.messageid);
          console.log($PilaMensajes.items);
          $ionicLoading.hide();
          console.log("ocultando el spinner");
          console.log("esta es la repuesta: ", response);
          console.log(jwt);
          if (jwt.statusCode != 200) {
            $message.alert(
              "No se pudo realizar la solicitud intenta mas tarde",
              "long",
              "bottom",
              function () {}
            );
            //problemas rechaza la Promesa
            reject(response);
          } else {
            //todo bien resuelve la Promesa
            resolve(response);
          }
        }); //cierre invocacion metodo del socket
      });
    };

    server.testConnect = function () {
      if (io.socket.isConnected()) {
        console.log("Cant't, already connected");
      } else {
        io.socket.reconnect();
      }
    };

    var tryReconnect = function () {
      console.log("Tratando de reconectar -", io.socket.isConnected());
      if (!io.socket.isConnected()) {
        $timeout(tryReconnect, 100);
        try {
          io.socket.reconnect();
        } catch (e) {
          console.log(e);
        }
      } else {
        console.log("Reconnected");
        $state.reload();
      }
    };

    server.testDisconnect = function () {
      if (!io.socket.isConnected()) {
        console.log("Cant't, already disconnected");
      } else {
        io.socket.disconnect();
      }
    };

    /**
     * Permite verificar si hay servicios por calificar.
     */
    server.xcalificar = function () {
      io.socket.get("/xcalificar/" + $usuario.id);
    };

    /**
     * Permite obtener las ciudades disponibles
     */
    server.ciudades = function (callback) {
      io.socket.get("/ciudades", function (res) {
        callback(res);
      });
    };

    /**
     * Permite obtener las coordenadas de una dirección.
     */
    server.coordenadas = function (direccion, callback) {
      io.socket.get("/coordenadas/" + direccion, function (res) {
        callback(res);
      });
    };

    /**
     * Permite obtener las categorías.
     */
    server.servicios = function (callback) {
      $ionicLoading.show();
      io.socket.get("/categorias", function (res) {
        for (var i in res) {
          res[i].imagen = _config.cms + res[i].imagen;
          res[i].imagen_fondo = _config.cms + res[i].imagen_fondo;
        }
        $ionicLoading.hide();
        callback(res);
      });
    };

    /**
     * Permite obtener las subcategorias.
     */
    server.categorias = function (id, callback) {
      console.log("subcategorias");
      server
        .enviarPeticionAPI("/sub-categorias/" + id, {}, "get")
        .then(function (res) {
          callback(res);
        }); //.catch(function(res){}) por si se necesita
    };

    /**
     * Permite obtener las promociones.
     */
    server.promociones = function (callback) {
      if (io.socket.isConnected()) {
        $ionicLoading.show();
        io.socket.get("/promociones", function (res) {
          for (var i in res) {
            res[i].imagen = _config.cms + res[i].imagen;
          }
          callback(res);
          $ionicLoading.hide();
        });
      } else {
        callback(null);
      }
    };

    /**
     * Permite obtener los banners.
     */
    server.banners = function (callback) {
      io.socket.get("/banners", function (res) {
        for (var i in res) {
          res[i].imagen = _config.cms + res[i].imagen;
        }
        callback(res);
      });
    };

    /**
     * Permite obtener los puntos más cercanos a la busqueda.
     */
    server.buscardor = function (opciones, callback) {
      io.socket.get(
        "/buscador/" +
          opciones.busqueda +
          "/" +
          opciones.latitud +
          "/" +
          opciones.longitud,
        function (res) {
          callback(res);
        }
      );
    };

    /**
     * Permite obtener los puntos más cercanos a la ubicación enviada.
     */
    server.puntos = function (opciones, callback) {
      server
        .enviarPeticionAPI(
          "/puntos/" +
            opciones.categoria +
            "/" +
            opciones.latitud +
            "/" +
            opciones.longitud,
          {},
          "get"
        )
        .then(function (res) {
          callback(res);
        })
        .catch(function (res) {
          callback(null);
        });
    };

    /**
     * Permite obtener los puntos más cercanos a la ubicación enviada por subcategoria.
     */
    server.puntosSubcategoria = function (opciones, callback) {
      io.socket.get(
        "/puntossubcategoria/" +
          opciones.categoria +
          "/" +
          opciones.latitud +
          "/" +
          opciones.longitud,
        function (res) {
          callback(res);
        }
      );
    };

    /**
     * Permite obtener la información y servicios de un punto.
     */
    server.punto = function (id, callback) {
      io.socket.get("/punto/" + id, function (res) {
        callback(res);
      });
    };

    /**
     * Permite enviar solicitud a un punto.
     */
    server.solicitar = function (solicitud, callback) {
      if (solicitud.precio == null) solicitud.descuento = 0;
      console.log(solicitud);
      server
        .enviarPeticionAPI("/solictiar", solicitud, "post")
        .then(function (res) {
          $message.alert(
            "Solicitud enviada, serás notificado de la respuesta. (+ Ver más)",
            "long",
            "bottom",
            function () {}
          );
          $state.go("dashboard.servicio");
        }); //.catch(function(res){}) por si se necesita
    };

    /**
     * Permite enviar solicitud a un punto.
     * parametro argumento id_servicio
     */
    server.cancelar = function (id_servicio, callback) {
      /* io.socket.post('/cancelar', { id: id_servicio }, function(response) {}); */
      server
        .enviarPeticionAPI("/cancelar", { id: id_servicio }, "post")
        .then(function (res) {
          callback();
          $message.alert(
            "Servicio cancelado.",
            "long",
            "bottom",
            function () {}
          );
        });
    };

    /**
     * Este metodo permite calificar un usaurio.
     */
    server.calificacion = function (datos, callback) {
      io.socket.post("/calificacion", datos, function () {
        callback();
      });
    };

    /**
     * Permite obtener los servicios.
     */
    server.serviciosUsuario = function (id, callback, cbupdate) {
      io.socket.on("updateservice", cbupdate);
      io.socket.get("/servicios-usuario/" + id, callback);
    };

    /**
     * Permite enviar solicitud a un punto.
     */
    server.historico = function (id, callback) {
      io.socket.get("/historico/" + id, function (response) {
        console.log("esta es el grandisimo id", id);
        callback(response);
      });
    };

    /**
     * Este metodo permite actualizar los datos de un usuario
     */
    server.actualizarDatos = function (usuario, callback) {
      io.socket.post(
        "/actualizar-datos/",
        {
          id: usuario.id,
          nombre: usuario.nombre,
          email: usuario.email,
          telefono: usuario.telefono,
        },
        function () {
          callback();
        }
      );
    };

    /**
     * Este metodo permite actualizar la contraseña de un usuario.
     */
    server.actualizarContrasena = function (usuario, callback) {
      io.socket.post("/actualizar-contrasena/", usuario, function () {
        callback();
      });
    };

    /**
     * Este metodo permite enviar el token de registro del usuario para la notificaciones.
     */
    server.registrarToken = function (id, token, dispositivo) {
      io.socket.post(
        "/registrar-token/",
        { id: id, token: token, dispositivo: dispositivo },
        function () {}
      );
    };

    server.xcalificar();
  });
