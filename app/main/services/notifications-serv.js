'use strict';
angular.module('main').factory('$push', function ($usuario, $server) {
 
    var service = {};   
    var push = null;
    var sendID = "989819467752";
    var sizeLimit = 10;
    var inited = false;
    var registro = {};

    service.registro = function (callback) {

        
        if (!window.cordova) return callback();
        document.addEventListener('deviceready', function () {
            //if (!window.cordova) return callback();
            push = PushNotification.init({
                android: {
                    forceShow: true
                },
                ios: {
                    alert: true,
                    badge: true,
                    sound: true,
                    clearBadge: true
                }
            });
            console.log("here is a token"); 
            console.log(push)
            push.on('registration', function (data) {

                inited = true;
                push.token = data.registrationId;
                console.log("here is a token",push.token);
                PushNotification.hasPermission(function () {
                    if ($usuario.id)
                        $server.registrarToken($usuario.id, push.token, ionic.Platform.platform());
                    callback();
                });
            });

            push.on('notification', function (data) {
            });

            push.getApplicationIconBadgeNumber(function (n) {
                service.badge = 0;
            }, function () {
                service.badge = 0;
            });
        });

    }

    return service;

});