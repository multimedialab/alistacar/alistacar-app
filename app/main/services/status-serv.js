'use strict';
angular.module('main').service('$status', function($ionicLoading, $ionicPopup, $rootScope) {

    var detection = {};
    var activo = true;
    var networkConnected = true;

    var serv = this;

    serv.checkConnection = function() {
        if (!window.cordova)
            return;
        var networkState = navigator.connection.type;
        var states = {};
        states[Connection.UNKNOWN] = 'UNKNOWN';
        states[Connection.ETHERNET] = 'ETHERNET';
        states[Connection.WIFI] = 'WIFI';
        states[Connection.CELL_2G] = 'CELL_2G';
        states[Connection.CELL_3G] = 'CELL_3G';
        states[Connection.CELL_4G] = 'CELL_4G';
        states[Connection.CELL] = 'CELL';
        states[Connection.NONE] = 'NONE';
        return states[networkState];
    }

    serv.killApp = function() {

    }

    document.addEventListener("deviceready", function() {
        $ionicLoading.hide();
        document.addEventListener("resume", function() {
            if (activo) return;
            //$ionicLoading.hide();

            activo = true;
            $rootScope.$broadcast('appforeground');
        }, false);

        document.addEventListener("pause", function() {
            if (!activo) return;

            activo = false;
            $rootScope.$broadcast('appbackground');
        }, false);

        window.addEventListener('online', function() {
            if (networkConnected) return;

            networkConnected = true;
            $ionicLoading.hide();
            $rootScope.$broadcast('networkonline');
        });

          window.addEventListener('offline', function() {
            if (!networkConnected) return;

            networkConnected = false;
            $ionicLoading.show({
                template: '<ion-spinner></ion-spinner> <br/> Necesitas internet para usar Alistacar<br/> Por favor revisa tu conexión'
            })
            $rootScope.$broadcast('networkoffline');
        }); 

    }, false);

});