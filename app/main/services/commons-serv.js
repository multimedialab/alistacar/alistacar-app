'use strict';
angular.module('main').service('$commons', function () {

  var service = this;

  service.apply = function ($scope) {
    if ($scope.$root && $scope.$root.$$phase != '$apply' && $scope.$root.$$phase != '$digest') {
      $scope.$apply();
    }
  }

  return service;

});
