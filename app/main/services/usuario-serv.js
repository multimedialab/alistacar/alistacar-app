'use strict';
angular.module('main').factory('$usuario', function($state, $localStorage, $ionicHistory) {

    var usuario = this;

    usuario.id = ($localStorage.usuario && $localStorage.usuario.id) ? $localStorage.usuario.id : null;
    usuario.nombre = ($localStorage.usuario && $localStorage.usuario.nombre) ? $localStorage.usuario.nombre : null;
    usuario.email = ($localStorage.usuario && $localStorage.usuario.email) ? $localStorage.usuario.email : null;
    usuario.telefono = ($localStorage.usuario && $localStorage.usuario.telefono) ? $localStorage.usuario.telefono : null;

    usuario.socket = io.socket;

    usuario.login = function(credenciales, callback) {
        io.socket.post('/login-user', credenciales, function(res) {
            if (!res)
                return callback(false);

            $localStorage.usuario = {};
            usuario.id = res.id_usuario;
            $localStorage.usuario.id = res.id_usuario;
            usuario.nombre = res.nombre;
            $localStorage.usuario.nombre = res.nombre;
            usuario.email = res.email;
            $localStorage.usuario.email = res.email;
            usuario.telefono = res.telefono;
            $localStorage.usuario.telefono = res.telefono;
            return callback(true);
        });
    }

    usuario.registro = function(datos, callback) {
        io.socket.post('/registro', datos, function(res) {
            if (!res)
                return callback(false);

            $localStorage.usuario = {};
            usuario.id = res.id_usuario;
            $localStorage.usuario.id = res.id_usuario;
            usuario.nombre = res.nombre;
            $localStorage.usuario.nombre = res.nombre;
            usuario.email = res.email;
            $localStorage.usuario.email = res.email;
            usuario.telefono = res.telefono;
            $localStorage.usuario.telefono = res.telefono;
            return callback(true);
        });
    }

    usuario.facebook = function(datos, callback) {
        io.socket.post('/facebook', datos, function(res) {
            if (!res)
                return callback(false);

            $localStorage.usuario = {};
            usuario.id = res.id_usuario;
            $localStorage.usuario.id = res.id_usuario;
            usuario.nombre = res.nombre;
            $localStorage.usuario.nombre = res.nombre;
            usuario.email = res.email;
            $localStorage.usuario.email = res.email;
            usuario.telefono = res.telefono;
            $localStorage.usuario.telefono = res.telefono;
            return callback(true);
        });
    }

    usuario.logout = function() {
        delete $localStorage.usuario;
        usuario.id = null;
        usuario.nombre = null;
        usuario.email = null;
        usuario.telefono = null;
        $ionicHistory.nextViewOptions({ disableBack: true });
        $ionicHistory.clearHistory();
        $ionicHistory.clearCache().then(function() {
            $state.go('login', {}, { reload: true });
        });
    }

    usuario.getAuth = function() {
        if ($localStorage.usuario)
            return true;
        else
            return false;
    }

    usuario.olvidoContrasena = function(email, callback) {
        io.socket.post('/olvidopassword', { 'email': email }, function(res, data) {
            console.log(res, data);
            if (data.statusCode === 500)
                callback(false);
            else
                callback(true)
        });
    }

    return usuario;

});