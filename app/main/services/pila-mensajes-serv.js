'use strict';
angular.module('main')
.service('$PilaMensajes', function ($log) {
  var  serv= this;
  serv.items = {};
  var id = 0;


  serv.agregar= function(item){
   id = serv.generarId();
   item.messageid = id;
   serv.items[id] = item;
    return id;
  }

  serv.eliminar= function(id){
    delete serv.items[id];
  }

  serv.generarId = function(){
    id = Date.now()+Math.floor(Math.random()*100);;
    return id;
  }

  serv.eliminarCola= function(){
    serv.items = {};
  }
});
