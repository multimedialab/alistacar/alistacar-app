"use strict";

angular
  .module("main", ["ionic", "ngCordova", "ui.router", "ngStorage"])
  .config(function ($stateProvider) {
    $stateProvider.state("main", { url: "/" });
  })
  .constant("_estados", {
    login: "Login",
    register: "Registro",
    perfil: "Perfil",
    servicio: "Servicios Activos",
    historico: "Historico de servicios",
    ubicacion: "Home",
    categorias: "Selección de Categoria",
    puntos: "Puntos de categoria ID: ",
    punto: "Punto de servicio ID: ",
  })
  .run(function (
    $state,
    $usuario,
    $ionicPlatform,
    $rootScope,
    _estados,
    $status,
    $push
  ) {
    //$state.go((!$usuario.getAuth()) ? "login" : "dashboard.categorias");

    if ($usuario.getAuth()) {
      $push.registro(function () {});
      setTimeout(function () {
        $state.go("dashboard.categorias");
      }, 100);
    } else {
      $state.go("login");
    }
    
    document.addEventListener(
      "deviceReady",
      function () {
        console.log("deviceReady");
        //window.ga.startTrackerWithId('UA-82813170-1');
        //window.ga.trackView((!$usuario.getAuth()) ? "Login" : "Home");

        if (ionic.Platform.platform() == "ios") navigator.splashscreen.hide();
        else
          setTimeout(function () {
            navigator.splashscreen.hide();
          }, 1000);
      },
      false
    );

    $rootScope.$on("$stateChangeSuccess", function (
      event,
      toState,
      toParams,
      fromState,
      fromParams
    ) {
      if (!window.ga) return;

      var state = toState.url.split("/")[1];
      var track = "";

      // if (state == 'puntos')
      //     window.ga.trackView(_estados[state] + toParams.id_categoria);
      // else if (state == 'punto')
      //     window.ga.trackView(_estados[state] + toParams.id_punto);
      // else
      //     window.ga.trackView(_estados[state]);
    });
  });
