'use strict';

describe('module: main, service: PilaMensajes', function () {

  // load the service's module
  beforeEach(module('main'));
  // load all the templates to prevent unexpected $http requests from ui-router
  beforeEach(module('ngHtml2Js'));

  // instantiate service
  var PilaMensajes;
  beforeEach(inject(function (_PilaMensajes_) {
    PilaMensajes = _PilaMensajes_;
  }));

  it('should do something', function () {
    expect(!!PilaMensajes).toBe(true);
  });

});
